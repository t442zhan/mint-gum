module.exports = {
  apps: [
    {
      name: "se464-lab",
      script: "./main.js",
      args: "rest sql",
      instances: 2,
      exec_mode: "cluster",
      env: {
        port: 3000,
      },
    },
  ],
};
