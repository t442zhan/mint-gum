require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  console.log("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  const randomIndex = Math.floor(Math.random() * response.Items.length);
  return response.Items[randomIndex];
};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {
  const id = uuid.v4();
  const command = new PutCommand({
    TableName: "Orders",
    Item: {
      user_id: order.user_id,
      id: id,
      products: order.products,
      total_amount: order.total_amount,
    },
  });

  const response = await docClient.send(command);

  const inserted_order = await queryOrderById(id);

  return inserted_order;
};

const updateUser = async (id, updates) => {
  // set #name = :namef, #email = :emailf, #password = :passwordf
  const update_expression = Object.keys(updates).filter((key) => key !== "id").map((key) => {
    return `#${key} = :${key}f`;
  });

  // create expression attribute names object which is in the form of { "#name": "name", "#email": "email", "#password": "password" }
  const expression_attribute_names = Object.keys(updates).filter((key) => key !== "id").reduce((acc, key) => {
    acc[`#${key}`] = key;
    return acc;
  }, {});
  
  // create expression attribute values object which is in the form of { ":namef": "name", ":emailf": "email", ":passwordf": "password" }
  const expression_attribute_values = Object.keys(updates).filter((key) => key !== "id").reduce((acc, key) => {
    acc[`:${key}f`] = updates[key];
    return acc;
  }, {});

  const command = new UpdateCommand({
    TableName: "Users",
    Key: {
      id: id,
    },
    UpdateExpression: "set " + update_expression.join(", "),
    ExpressionAttributeNames: expression_attribute_names,
    ExpressionAttributeValues: expression_attribute_values,
    ReturnValues: "ALL_NEW",
  });

  const response = await docClient.send(command);
  return response.Attributes;
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
