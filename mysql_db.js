require("dotenv").config();
const logger = require("./logger");
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  //  try {
  //    await connect();
  //    logger.info("Database connected!");
  //  } catch (e) {
  //    console.error(e);
  //  }
};

const queryProductById = async (productId) => {
  const x = await mysql.query(`SELECT *
                                FROM products
                                WHERE id = "${productId}";`);
  return x[0][0];   //mysql returns [rows, fields] and we want rows[0]
};

const queryRandomProduct = async () => {

  const x = await mysql.query(`SELECT *
                              FROM products
                              ORDER BY RAND()
                              LIMIT 1`);
  return x[0][0];
};

const queryAllProducts = async () => {
  const x = await mysql.query(`SELECT *
    FROM products`)
  return x[0];
};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

//insertOrder can take an order which has multiple products
const insertOrder = async (order) => {
  const orderId = uuid.v4();
  const orderItems = order.products.map(product => {
    return `("${uuid.v4()}", "${orderId}", "${product.product_id}", ${product.quantity})`
  })

  await mysql.query(`INSERT INTO orders (id, user_id, total_amount)
                        VALUES ("${orderId}", "${order.user_id}", ${order.total_amount})`)
  await mysql.query(`INSERT INTO order_items (id, order_id, product_id, quantity)
                        VALUES ${orderItems.join(",")}`)

  // Query the inserted order and return it
  const insertedOrder = await mysql.query(`SELECT id, user_id, total_amount FROM orders WHERE id = "${orderId}"`);
  const insertedProducts = await mysql.query(`SELECT product_id, quantity FROM order_items WHERE order_id = "${orderId}"`);

  return {
    id: insertedOrder[0],
    user_id: insertedOrder[1],
    total_amount: insertedOrder[2],
    products: insertedProducts.map(product => ({
      product_id: product[0],
      quantity: product[1]
    }))
  }
};

const updateUser = async (id, updates) => {
  const query_set_params = [
    updates.name && `name="${updates.name}"`,
    updates.email && `email="${updates.email}"`,
    updates.password && `password="${updates.password}"`
  ].filter(Boolean)

  return (await mysql.query(`UPDATE users SET ${query_set_params.join(", ")}
                              WHERE id = "${id}"`))[0];
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
